import 'dart:io';
void main() {
   const phi = 3.14;
  stdout.write("Masukkan panjang jari-jari lingkaran: ");
  String? s = stdin.readLineSync();
  int r = 0;
  if (s != null){
    r = int.parse(s);
  }
  double luas = phi * r * r;
  print("Luas lingkaran adalah $luas");
}