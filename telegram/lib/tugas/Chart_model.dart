class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel({required this.name, required this.message, required this.time, required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Adelia',
      message: 'Apa kabar diana?',
      time: '16.04',
      profileUrl:
          'https://obs.line-scdn.net/0h1XZCb4pdbndnOnkKV1IRIF9sYgZUXHR-RVUlEkE-ZRNOFnwoC1k9FBZoMltDXnt2RwgkQks-YhdCCHlzWg/w1200'),
  ChartModel(
      name: 'Azmi',
      message: 'Ayo aku akan segera berangkat',
      time: '15.59',
      profileUrl:
          'https://akcdn.detik.net.id/visual/2021/02/23/5-tips-outift-hijab-untuk-cewek-pendek-agar-terlihat-lebih-tinggi_169.jpeg?w=700&q=90'),
  ChartModel(
      name: 'Nendra',
      message: 'Tugas mu sudahkah selesai diana?',
      time: '14.16',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/02/23/celana-model-widepinterestcom.jpeg?w=620&q=90'),
  ChartModel(
      name: 'Rosita',
      message: 'Dimana kamu?',
      time: '14.01',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/02/23/crop-toppinterestcom.jpeg?w=620&q=90'),
  ChartModel(
      name: 'Ifa',
      message: 'Aku di Mojokerto',
      time: '13.59',
      profileUrl:
          'https://scontent.fsub8-1.fna.fbcdn.net/v/t1.6435-9/121644128_212958757015455_3612156851897016921_n.jpg?stp=cp0_dst-jpg_e15_p480x480_q65&_nc_cat=100&ccb=1-5&_nc_sid=8024bb&_nc_ohc=HuLGMk81PH4AX9oNDdU&_nc_ht=scontent.fsub8-1.fna&oh=00_AT8NJ9mTo4cMkEOucy9xHHDyK-yIACuCDtYGdAiry4U4tg&oe=625D6501'),
  ChartModel(
      name: 'Rudy',
      message: 'Ayo beli makan bersama',
      time: '13.47',
      profileUrl:
          'https://pbs.twimg.com/media/FKwt7u1UcAY2yd0?format=jpg&name=medium'),
  ChartModel(
      name: 'Beng-Beng',
      message: 'Aku VC sekarang yah',
      time: '13.00',
      profileUrl:
          'https://obs.line-scdn.net/0h14QeLY7FbhtsHELwnMoRTFZKbXRfcH0YCCo_BTxyMC8RJXsaBXMhLkAbNi1GfClFAi4leUscdSpHfiFOVXwh/w1200'),
  ChartModel(
      name: 'Lukman',
      message: 'Gimana errornya sudah bisa belum?',
      time: '12.21',
      profileUrl:
          'https://static.indiemarket.news/static-content/uploads/2020/09/OOTD-Rizki-Nazar.jpg'),
  ChartModel(
      name: 'Nila',
      message: 'Jom berangkat',
      time: '11.41',
      profileUrl:
          'https://scontent.fsub8-1.fna.fbcdn.net/v/t1.6435-9/121677347_212958733682124_4365406849694474310_n.jpg?stp=cp0_dst-jpg_e15_p480x480_q65&_nc_cat=100&ccb=1-5&_nc_sid=8024bb&_nc_ohc=ad8eQvQM84MAX-7t1dJ&_nc_ht=scontent.fsub8-1.fna&oh=00_AT_2EBqrJxZH1bEl6M3PK4ujhCkKOeCj94G_Z4iS28o1JA&oe=625E658F'),
  ChartModel(
      name: 'adi',
      message: 'Kapan Kamu pulang?',
      time: '20 March 2022',
      profileUrl:
          'https://pbs.twimg.com/media/FKwt7WoVcAAwlnk?format=jpg&name=medium'),
  ChartModel(
      name: 'Ridya',
      message: 'Diana',
      time: '20 March 2022',
      profileUrl:
          'https://scontent-sin6-2.xx.fbcdn.net/v/t1.6435-9/121774847_212958750348789_2592387790361404201_n.jpg?stp=cp0_dst-jpg_e15_p480x480_q65&_nc_cat=108&ccb=1-5&_nc_sid=8024bb&_nc_ohc=GyRpUKFjbxMAX-WYsTZ&_nc_ht=scontent-sin6-2.xx&oh=00_AT9PdTle5Qeqh6rtgGkEsgkjRGX3Ysryey-ohZWn7PdGsg&oe=625D2CC1'),
  ChartModel(
      name: 'Ridan',
      message: 'Aku otw',
      time: '20 March 2022',
      profileUrl:
          'https://pbs.twimg.com/media/FKwt61CUYAEbmYL?format=jpg&name=large'),
  ChartModel(
      name: 'Hyuan',
      message: 'Besok nebeng ke kampus',
      time: '20 March 2022',
      profileUrl:
          'https://scontent-sin6-2.xx.fbcdn.net/v/t1.6435-9/122010428_212958463682151_3284850664424751503_n.jpg?stp=cp0_dst-jpg_e15_p480x480_q65&_nc_cat=105&ccb=1-5&_nc_sid=8024bb&_nc_ohc=3-k35mBqaz8AX_gIbBT&_nc_ht=scontent-sin6-2.xx&oh=00_AT_AiwxJPIXX0MzeRBo6VaqRIl2pEmjJnLbwVJ1vQcbvRg&oe=625D9AE2'),
  ChartModel(
      name: 'Boy',
      message: 'hello diana',
      time: '25 January 2022',
      profileUrl:
          'https://pbs.twimg.com/media/FKwt6SYVgAENAUR?format=jpg&name=medium'),
  ChartModel(
      name: 'Rafli',
      message: 'Aku di kost',
      time: '20 January 2022',
      profileUrl:
          'https://pbs.twimg.com/media/FNJGBv0UYAIgYTk?format=jpg&name=medium'),
];
