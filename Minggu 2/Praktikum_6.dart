import 'dart:io';

void main() {
  print("Selamat datang,\nMasukkan Nickname anda ! : ");
  var nama = stdin.readLineSync();
  print(
      "Daftar peran:\n1. Penyihir | 2. Guard | 3. Werewolf\nMasukkan peran : ");
  var peran = stdin.readLineSync();

  if (nama == "") {
    print("Nama harus diisi!");
  } else if (peran == "") {
    print("Halo" + nama.toString() + ", Pilih Peranmu untuk memulai game!");
  } else if (peran == "1") {
    print("Selamat datang di Dunia Werewolf, " +
        nama.toString() +
        ".\nHalo Penyihir " +
        nama.toString() +
        ", kamu dapat melihat siapa yang menjadi werewolf.");
  } else if (peran == "2") {
    print("Selamat datang di Dunia Werewolf, " +
        nama.toString() +
        ".\nHalo Guard " +
        nama.toString() +
        ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (peran == "3") {
    print("Selamat datang di Dunia Werewolf, " +
        nama.toString() +
        ".\nHalo Werewolf " +
        nama.toString() +
        ",  Kamu akan memakan mangsa setiap malam.");
  }
}
