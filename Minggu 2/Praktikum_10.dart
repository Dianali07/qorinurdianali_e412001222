void main() {
  var limit = 5;
  for (var i = 1; i <= limit; i++) {
    var space = " ";
    for (var j = 1; j <= i; j++) {
      space += "#";
    }
    print(space);
  }
}