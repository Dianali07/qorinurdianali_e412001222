import 'dart:io';

void main() {
  var hari = 20;
  var bulan = 03;
  var tahun = 2001;

  if (hari <= 0 ||
      hari > 31 && bulan <= 0 ||
      bulan >= 13 && tahun < 1900 ||
      tahun > 2200) {
    print("Invalid");
  } else {
    switch (bulan) {
      case 1:
        {
          print("${hari.toString()} Januari ${tahun.toString()}");
          break;
        }
      case 2:
        {
          print("${hari.toString()} Februari ${tahun.toString()}");
          break;
        }
      case 3:
        {
          print("${hari.toString()} Maret ${tahun.toString()}");
          break;
        }
      case 4:
        {
          print("${hari.toString()} April ${tahun.toString()}");
          break;
        }
      case 5:
        {
          print("${hari.toString()} Mei ${tahun.toString()}");
          break;
        }
      case 6:
        {
          print("${hari.toString()} Juni ${tahun.toString()}");
          break;
        }
      case 7:
        {
          print("${hari.toString()} Juli ${tahun.toString()}");
          break;
        }
      case 8:
        {
          print("${hari.toString()} Agustus ${tahun.toString()}");
          break;
        }
      case 9:
        {
          print("${hari.toString()} September ${tahun.toString()}");
          break;
        }
      case 10:
        {
          print("${hari.toString()} Oktober ${tahun.toString()}");
          break;
        }
      case 11:
        {
          print("${hari.toString()} November ${tahun.toString()}");
          break;
        }
      case 12:
        {
          print("${hari.toString()} Desember ${tahun.toString()}");
          break;
        }
    }
  }
}
