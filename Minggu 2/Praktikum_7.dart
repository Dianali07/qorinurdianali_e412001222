import 'dart:io';

void main() {
  print(
      "Daftar peran:\n1. Senin | 2. Selasa | 3. Rabu | 4. Kamis | 5. Jumat | 6. Sabtu | 7. Minggu\nPilih hari anda : ");
  var peran = stdin.readLineSync();
  if (peran == "1") {
    print(
        "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja");
  } else if (peran == "2") {
    print(
        "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati");
  } else if (peran == "3") {
    print(
        "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain");
  } else if (peran == "4") {
    print(
        "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri");
  } else if (peran == "5") {
    print("Hidup tak selamanya tentang pacar");
  } else if (peran == "6") {
    print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan");
  } else if (peran == "7") {
    print(
        "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani");
  }
}
